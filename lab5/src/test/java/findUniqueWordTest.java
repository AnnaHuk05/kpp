import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
public class findUniqueWordTest {
    @Test
    public void TestRemoveWorldsByLengthWithConsonantNullString() {
        String text = null;
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> {
            HashSet<String> result = StringHandler.findUniqueWords(text);
        });
    }
    @Test
    public void testSeparateSentencesWithRegularText() {
        String text = "This is a regular text. With sentences. And periods.";
        String[] expectedSentences = { "This is a regular text", "With sentences", "And periods" };
        String[] actualSentences = StringHandler.separateSentences(text);
        Assertions.assertArrayEquals(expectedSentences, actualSentences);
    }

    @Test
    public void testSeparateSentencesWithExclamationMark() {
        String text = "Hello! How are you! I am good!";
        String[] expectedSentences = { "Hello", "How are you", "I am good" };
        String[] actualSentences = StringHandler.separateSentences(text);
        Assertions.assertArrayEquals(expectedSentences, actualSentences);
    }

    @Test
    public void testSeparateSentencesWithQuestionMark() {
        String text = "What is this? Where are you going?";
        String[] expectedSentences = { "What is this", "Where are you going" };
        String[] actualSentences = StringHandler.separateSentences(text);
        Assertions.assertArrayEquals(expectedSentences, actualSentences);
    }

    @Test
    public void testSeparateSentencesWithEllipsis() {
        String text = "This is the end of the story... Or is it?";
        String[] expectedSentences = { "This is the end of the story", "Or is it" };
        String[] actualSentences = StringHandler.separateSentences(text);
        Assertions.assertArrayEquals(expectedSentences, actualSentences);
    }

    @Test
    public void testSeparateSentencesWithNoPeriodInLastSentence() {
        String text = "This is a regular text. One more sentence. No period at the end";
        String[] expectedSentences = { "This is a regular text", "One more sentence", "No period at the end" };
        String[] actualSentences = StringHandler.separateSentences(text);
        Assertions.assertArrayEquals(expectedSentences, actualSentences);
    }

    @Test
    public void testSeparateSentencesWithLowerCaseStartingSentence() {
        String text = "this is a regular text. With sentences. And periods.";
        String[] expectedSentences = { "this is a regular text", "With sentences", "And periods" };
        String[] actualSentences = StringHandler.separateSentences(text);
        Assertions.assertArrayEquals(expectedSentences, actualSentences);
    }

    @Test
    public void testSeparateSentencesWithEmptyText() {
        String text = "";
        String[] expectedSentences = { "" };
        String[] actualSentences = StringHandler.separateSentences(text);
        Assertions.assertArrayEquals(expectedSentences, actualSentences);
    }

    @Test
    public void testSeparateSentencesWithAllRepeatedWords() {
        String text = "This is a test. This is a test. This is a test.";
        String[] expectedSentences = { "This is a test", "This is a test", "This is a test" };
        String[] actualSentences = StringHandler.separateSentences(text);
        assertArrayEquals(expectedSentences, actualSentences);
    }

    @Test
    public void testFindUniqueWords() {
        String text = "This is a test. Test of a function. Test example.";
        HashSet<String> expectedUniqueWords = new HashSet<>();
        expectedUniqueWords.add("this");
        expectedUniqueWords.add("is");
        HashSet<String> actualUniqueWords = StringHandler.findUniqueWords(text);
        assertEquals(expectedUniqueWords, actualUniqueWords);
    }

        @Test
        public void testEmptyText() {
            String text = "";
            HashSet<String> actualResult = StringHandler.findUniqueWords(text);
            Assertions.assertTrue(actualResult.isEmpty());
        }

        @Test
        public void testAllWordsRepeated() {
            String text = "This is a sample text. This is a sample text.";
            HashSet<String> result = StringHandler.findUniqueWords(text);
            Assertions.assertTrue(result.isEmpty());
        }

        @Test
        public void testUniqueWordAfterColon() {
            String text = "This is a sample text: wow. This is a sample wow.";
            HashSet<String> result = StringHandler.findUniqueWords(text);
            HashSet<String> expectedResult= new HashSet<>();
            expectedResult.add("text");
            Assertions.assertTrue(result.contains("text"));

            assertEquals(expectedResult,result);

        }

        @Test
        public void testSentenceCaseMismatch() {
            String text = "This is a sample text. Now this is another text.";
            HashSet<String> result = StringHandler.findUniqueWords(text);
            Assertions.assertFalse(result.contains("this"));


        }

        @Test
        public void testSingleUniqueWord() {
            String text = "This is a sample text. This is not a text.";
            HashSet<String> result = StringHandler.findUniqueWords(text);
            assertEquals(1, result.size());
            Assertions.assertTrue(result.contains("sample"));
        }

        @Test
        public void testMultipleUniqueWords() {
            String text = "Fruits: apple, banana, cherry. This is a sample text for fruits.";
            HashSet<String> result = StringHandler.findUniqueWords(text);
            HashSet<String> expectedResult = new HashSet<>();
            expectedResult.add("apple");
            expectedResult.add("banana");
            expectedResult.add("cherry");

            assertEquals(result,expectedResult);


        }
    }



