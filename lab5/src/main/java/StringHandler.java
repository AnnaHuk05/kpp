

import java.util.HashSet;
import java.util.regex.*;


public class StringHandler {
    public static String[] separateSentences(String text) {
        if (text == null) {
            throw new IllegalArgumentException("Text cannot be null");
        }
        String[] sentences = text.split("([.!?]+|\\.\\.\\.)");
        for (int i = 0; i < sentences.length; i++) {
            sentences[i] = sentences[i].trim();
        }
        String firstSentence = sentences[0];

       return sentences;
    }
    public static  HashSet<String> findUniqueWords(String text) {
        if(text == null){
            throw new IllegalArgumentException("Text cannot be null");
        }
        String regex = "\\b(\\w+)\\b";
        Pattern pattern = Pattern.compile(regex);
        String[] sentenses = separateSentences(text);
        Matcher matcher = pattern.matcher(sentenses[0]);
        HashSet<String> firstSentenceWords = new HashSet<>();
        while (matcher.find()) {
            String word = matcher.group(1).toLowerCase();
            firstSentenceWords.add(word);
        }

        for (int i = 1; i < sentenses.length; i++) {
           matcher = pattern.matcher(sentenses[i]);

            while (matcher.find()) {
                String word = matcher.group(1).toLowerCase();
                firstSentenceWords.remove(word);
            }
        }
        HashSet<String> uniqueWords=new HashSet<String>();
        for(String word:firstSentenceWords)
        {
            if(word!=" ")
                uniqueWords.add(word);
        }
        return  firstSentenceWords;

    }
}

