import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class AppManager {
    public static void run()
    {
        System.out.println("Enter text");
        Scanner scanner = new Scanner(System.in);
        StringBuilder text = new StringBuilder();
        String line;

        while (true) {
            line = scanner.nextLine();
            if (line.isEmpty()) {
                break;
            }
            text.append(line).append("\n");
        }

        HashSet<String> uniqueWords = StringHandler.findUniqueWords(text.toString());
        if (uniqueWords.isEmpty()) {
            System.out.println("\nВсі слова першого речення присутні в інших реченнях.\n");
        } else {
            System.out.println("\nСлова, які відсутні в інших реченнях:\n");
            for (String word : uniqueWords) {
                System.out.println(word);
            }
        }
    }
}
