

import lombok.RequiredArgsConstructor;

import java.util.concurrent.ThreadLocalRandom;

@RequiredArgsConstructor
public class Client extends Thread {
    private final Bank bank;
    private int money = 0;
    private boolean chill;

    synchronized public int AmountOfMoney() {
        return money;
    }

    private void simulateWork() {
        var delay = 1000000000;
        for (int i = 0; i < delay; i++) {
            char work = "abcdef".charAt(i % 6);
        }
    }

    private void checkChill() {
        try {
            synchronized (this) {
                while (chill) {
                    wait();
                }
            }
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + "thread can`t wait");
        }
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            int amount = ThreadLocalRandom.current().nextInt(100, 300);
            if(amount < bank.getTotalMoney()) {
                bank.withdrawCash(amount);
                money += amount;
            }
            simulateWork();
            checkChill();

            int returnAmount = ThreadLocalRandom.current().nextInt(100, 300);
            if(returnAmount <= money) {
                bank.depositeCash(returnAmount);
                money -= returnAmount;
            }
            simulateWork();
            checkChill();
        }
    }

    synchronized public void suspendClient() {
        chill = true;
        System.out.println("suspend " + Thread.currentThread().getName());
    }

    synchronized public void resumeClient() {
        chill = false;
        System.out.println("resume " + Thread.currentThread().getName());
        notify();
    }
}
