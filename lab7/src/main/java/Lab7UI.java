import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Lab7UI {
    private Bank bank = new Bank(new Semaphore(0), 0);
    private final ArrayList<Client> clients = new ArrayList<>();

    private JTable clientsTable;
    private JTextField ClientsF;
    private JTextField CashF;
    private JTextField MoneyInBankF;
    private JButton runSelectedButton;
    private JButton suspendSelectedButton;
    private JPanel MainPanel;
    private JScrollPane ClientsTableScroll;

    public Lab7UI() {
        ClientsF.addActionListener(e -> updateAll());
        CashF.addActionListener(e -> updateAll());

        runSelectedButton.addActionListener(e -> {
            var rows = clientsTable.getSelectedRows();
            for (int row : rows) {
                var reader = clients.get(row);
                if (reader.isAlive()) {
                    reader.resumeClient();
                } else {
                    reader.start();
                }
            }
        });

        suspendSelectedButton.addActionListener(e -> {
            var rows = clientsTable.getSelectedRows();
            for (int row : rows) {
                var reader = clients.get(row);
                reader.suspendClient();
            }
        });


        new Thread(() -> {
            while (true) {
                clientsTable.updateUI();
                MoneyInBankF.setText(String.valueOf(bank.getTotalMoney()));
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException in ui updater");
                }
            }
        }).start();
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("UserInterface");
        frame.setContentPane(new Lab7UI().MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(0,0,2000,2000);
        frame.pack();
        frame.setVisible(true);
    }

    public void updateAll() {
        var cash = CashF.getText();
        if (cash.isEmpty()) {
            cash = "0";
        }
        bank = new Bank(new Semaphore(Integer.parseInt(cash)/100), Integer.parseInt(cash));

        clients.forEach(r -> {
            if (r.isAlive()) r.interrupt();
        });
        clients.clear();
        var clientsCountString = ClientsF.getText();
        if (clientsCountString.isEmpty()) {
            clientsCountString = "0";
        }
        int count = Integer.parseInt(clientsCountString);
        for (int i = 0; i < count; i++) {
            var client = new Client(bank);
            client.setPriority(new Random().nextInt(1,2));
            clients.add(client);
        }
    }

    private void createUIComponents() {
        clientsTable = new JTable(new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return clients.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public String getColumnName(int column) {
                switch (column) {
                    case 0:
                        return "Name";
                    case 1:
                        return "State";
                    case 2:
                        return "Money";
                    default:
                        return "";
                }
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                var reader = clients.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return reader.getName();
                    case 1:
                        return reader.getState();
                    case 2:
                        return "$" + reader.AmountOfMoney();
                    default:
                        return "";
                }
            }
        });
        clientsTable.setFont(new Font("Consolas", Font.PLAIN, 18));
        clientsTable.getTableHeader().setFont(new Font("Consolas", Font.BOLD, 18));
        clientsTable.setRowHeight(30);
        clientsTable.getColumnModel().getColumn(1).setPreferredWidth(150);
        
    }
    }

