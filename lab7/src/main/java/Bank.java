import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.concurrent.Semaphore;

@Getter
public class Bank {
    private final Semaphore semaphore;
    private int totalMoney;

    public Bank(Semaphore semaphore, int totalMoney) {
        this.semaphore = semaphore;
        this.totalMoney = totalMoney;
    }

    public void withdrawCash(int amount) {
        semaphore.acquireUninterruptibly();
        synchronized (this) {
            totalMoney -= amount;
        }
        System.out.println("occupied " + Thread.currentThread().getName());
    }

    public void depositeCash(int amount) {
        synchronized (this) {
            totalMoney += amount;
        }
        semaphore.release();
        System.out.println("release " + Thread.currentThread().getName());
    }
}
