import java.util.ArrayList;
import java.util.List;

public class FlightProssesor {

    public void printPaths(List<List<Integer>> paths, Graph graph) {
        for (List<Integer> path : paths) {
            List<Flight> flights = new ArrayList<>();
            for (int vertex : path) {
                flights.add(graph.getFlight(vertex));
            }
            System.out.printf("\n%-15s %-15s %-40s %-40s %-10s\n", "Departure", "Destination", "Depart Time", "Arrival Time", "Cost");
            printFlights(flights);
        }
    }

    public void printFlights(List<Flight> flights) {
        for (Flight flight : flights) {
            System.out.println(flight.toString());
        }
    }
}
