import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class FlightGenerator {
    public static List<Flight> createFligths()
    {
        Airport airportA = new Airport(1, "CityA", 40.7128, -74.0060, ZoneId.of("Europe/London"));
        Airport airportB = new Airport(2, "CityB", 34.0522, -118.2437, ZoneId.of("America/New_York"));
        Airport airportC = new Airport(3, "CityC", 51.5074, -0.1278, ZoneId.of("UTC"));
        Airport airportD = new Airport(4, "CityD", 48.8566, 2.3522, ZoneId.of("GMT+02:00"));
        Airport airportE = new Airport(5, "CityE", 35.682839, 139.759455,ZoneId.ofOffset("GMT", ZoneOffset.ofHours(5)));


        Aircraft aircraft1 = new Aircraft("Aircraft1", 100,500);
        Aircraft aircraft2 = new Aircraft("Aircraft2", 120,600);
        Aircraft aircraft3 = new Aircraft("Aircraft3", 125,550);
        Aircraft aircraft4 = new Aircraft("Aircraft4", 140,700);


        ZonedDateTime departureTime1 = ZonedDateTime.of(2023, 10, 30, 8, 0, 0, 0, airportA.getZoneId());
        ZonedDateTime departureTime2 = ZonedDateTime.of(2023, 11, 1, 12, 30, 0, 0, airportB.getZoneId());
        ZonedDateTime departureTime3 = ZonedDateTime.of(2023, 11, 5, 15, 30, 0, 0, airportC.getZoneId());
        ZonedDateTime departureTime4 = ZonedDateTime.of(2023, 11, 10, 9, 15, 0, 0, airportA.getZoneId());
        ZonedDateTime departureTime5 = ZonedDateTime.of(2023, 11, 15, 8, 45, 0, 0, airportB.getZoneId());
        ZonedDateTime departureTime6 = ZonedDateTime.of(2023, 11, 20, 12, 0, 0, 0, airportC.getZoneId());
        ZonedDateTime departureTime7 = ZonedDateTime.of(2023, 11, 25, 14, 30, 0, 0, airportD.getZoneId());
        ZonedDateTime departureTime8 = ZonedDateTime.of(2023, 11, 30, 10, 15, 0, 0, airportE.getZoneId());
        ZonedDateTime departureTime9 = ZonedDateTime.of(2023, 12, 5, 7, 0, 0, 0, airportD.getZoneId());



        List<Flight> flights = new ArrayList<Flight>();

        flights.add(new Flight(airportA, airportB, aircraft1, departureTime1));
        flights.add(new Flight(airportB, airportC, aircraft2, departureTime2));
        flights.add(new Flight(airportC, airportD, aircraft2, departureTime3));
        flights.add(new Flight(airportA, airportE, aircraft1, departureTime2));
        flights.add(new Flight(airportB, airportD, aircraft3, departureTime4));
        flights.add(new Flight(airportD, airportE, aircraft4, departureTime3));
        flights.add(new Flight(airportC, airportB, aircraft3, departureTime2));
        flights.add(new Flight(airportE, airportC, aircraft1, departureTime5));
        flights.add(new Flight(airportD, airportC, aircraft4, departureTime6));
        flights.add(new Flight(airportB, airportA, aircraft2, departureTime7));
        flights.add(new Flight(airportA, airportD, aircraft3, departureTime8));
        flights.add(new Flight(airportE, airportA, aircraft4, departureTime9));
        return flights;
    }
}
