import java.time.ZoneId;

public class Airport {
        private int id;
        private String city;
        private double latitude;
        private double longitude;
        private ZoneId zone;


        public Airport(int id, String city, double latitude, double longitude, ZoneId zone) {
            this.id = id;
            this.city = city;
            this.latitude = latitude;
            this.longitude = longitude;
            this.zone =zone;

        }

        public int getId() {
            return id;
        }

        public String getCity() {
            return city;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public ZoneId getZoneId() {
            return zone;
        }

        @Override
        public String toString() {
            return "Airport{" +
                    "name='" + id + '\'' +
                    ", city='" + city +
                    '}';
        }
    }

