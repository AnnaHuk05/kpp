import java.time.LocalDateTime;

public class FligthInfoCalculator {
    public static double calculateDistance(Airport airport1, Airport airport2) {
        double lat1 = Math.toRadians(airport1.getLatitude());
        double lon1 = Math.toRadians(airport1.getLongitude());
        double lat2 = Math.toRadians(airport2.getLatitude());
        double lon2 = Math.toRadians(airport2.getLongitude());

        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;

        double a = Math.pow(Math.sin(dlat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double radius = 6371.0;

        double distance = radius * c;

        return distance;
    }
    public static double calculatePrice(Aircraft aircraft, double distance) {
        double pricePerKilometer = aircraft.getPricePerKilometer();
        return pricePerKilometer * distance;
    }

}
