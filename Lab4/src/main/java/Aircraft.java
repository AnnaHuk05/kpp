public class Aircraft {
    private String model;
    private double pricePerKilometer;
    private double speed;

    public Aircraft(String model, double pricePerKilometer, double speed) {
        this.model = model;
        this.pricePerKilometer = pricePerKilometer;
        this.speed = speed;
    }

    public String getModel() {
        return model;
    }

    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    public double getSpeed() {
        return speed;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "model='" + model + '\'' +
                ", pricePerKilometer=" + pricePerKilometer +
                ", speed=" + speed +
                '}';
    }
}
