import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;

public class AppUtility {
      public static void runApp() {
            List<Flight> fligths = FlightGenerator.createFligths();
            Scanner scanner = new Scanner(System.in);
            FlightProssesor flightProssesor = new FlightProssesor();
            System.out.println("Enter criteria with higher priority for you(cost/duration)");
            String weightPriority = scanner.nextLine();
            Graph graph = buildGraph(fligths, 100, weightPriority);

            while (true) {
                  System.out.println("1. See airport traffic ");
                  System.out.println("2. Choose flights by departure/destination/date");
                  System.out.println("3. Best flights for chosen route");


                  int choice = scanner.nextInt();
                  scanner.nextLine();
                  switch (choice) {
                        case 1:
                              System.out.println("Choose city");
                              String airport = scanner.nextLine();
                              graph.printFlightsByAirport(airport);
                              break;
                        case 2:
                              System.out.print("Enter departure: ");
                              String departure  = scanner.nextLine();
                              System.out.print("Enter destination: (or enter no)");
                              String  destination = scanner.nextLine();
                              System.out.print("Enter your preferred departure day: (yyyy-mm-dd)");
                              String answer = scanner.nextLine();
                              DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                              try{
                                    LocalDate date = LocalDate.parse(answer, formatter);
                                    graph.printFlightsByParameters(departure,destination, date);
                              } catch (java.time.format.DateTimeParseException e) {
                                    System.out.println("Невірний формат дати. Використовуйте рррр-мм-дд.");
                              }
                        case 3:
                              System.out.print("Enter departure");
                              departure= scanner.nextLine();
                              System.out.print("Enter destination:");
                              destination = scanner.nextLine();
                              flightProssesor.printPaths(graph.findBestRoutes(departure,destination,weightPriority),graph);
                        default:
                              System.out.println("Enter number");
                  }
            }
      }
                  public static Graph buildGraph(List<Flight> flights, int numVertices, String weightPriority) {
                  Graph graph = new Graph(numVertices);

                  for (int i = 0; i < flights.size(); i++) {
                        for (int j = 0; j < flights.size(); j++) {
                              if (i != j) {
                                    Flight flight1 = flights.get(i);
                                    Flight flight2 = flights.get(j);

                                    if (flight1.getArrivalAirport().getId() == flight2.getDepartureAirport().getId()) {
                                          Graph.Edge edge = new Graph.Edge(j, flight2, weightPriority,graph.calculateLayoverTime(flight1,flight2));
                                          graph.addEdge(i, j, edge);
                                    }
                              }
                        }
                  }
                  return graph;
            }
      }


