import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;

public class Flight {
    private Airport departureAirport;
    private Airport arrivalAirport;
    private Aircraft aircraft;
    private ZonedDateTime departureTime;
    private ZonedDateTime arrivalTime;
    private double cost;

    public Flight(Airport departureAirport, Airport arrivalAirport, Aircraft aircraft, ZonedDateTime departureTime) {
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.aircraft = aircraft;
        this.departureTime = departureTime;
        this.cost = calculatePrice();

        double distance = FligthInfoCalculator.calculateDistance(departureAirport, arrivalAirport);
        double speed = aircraft.getSpeed();
        double hoursRequired = distance / speed;
        long minutesRequired = Math.round(hoursRequired * 60);
        this.arrivalTime = departureTime.plusMinutes(minutesRequired);
    }

    public Airport getDepartureAirport() {
        return departureAirport;
    }

    public Airport getArrivalAirport() {
        return arrivalAirport;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public ZonedDateTime getDepartureTime() {
        return departureTime;
    }

    public ZonedDateTime getArrivalTime() {
        return arrivalTime;
    }

    public double getCost() {
        return cost;
    }

    public double calculatePrice() {
        double distance = FligthInfoCalculator.calculateDistance(departureAirport, arrivalAirport);
        return FligthInfoCalculator.calculatePrice(aircraft,distance);
    }
    @Override
    public String toString() {
        return String.format("%-15s %-15s %-40s %-40s %-10s",
                departureAirport.getCity(),
                arrivalAirport.getCity(),
                departureTime,
                arrivalTime,
                cost);
    }
}
