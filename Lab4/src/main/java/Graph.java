import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.LongAccumulator;

public class Graph {
    public int numVertices;
    public List<List<Edge>> adjacencyList;
    private final Map<Integer, List<Edge>> removedEdges;

    public Graph(int numVertices) {
        this.numVertices = numVertices;
        this.adjacencyList = new ArrayList<>();
        this.removedEdges = new HashMap<>();

        for (int i = 0; i < numVertices; i++) {
            this.adjacencyList.add(new LinkedList<>());
        }
    }
    public void addEdge(int from, int to, Edge edge) {
        adjacencyList.get(from).add(edge);
    }

    public void removeEdge(int from, int to) {
        Iterator<Edge> iterator = adjacencyList.get(from).iterator();
        while (iterator.hasNext()) {
            Edge edge = iterator.next();
            if (edge.vertex == to) {
                removedEdges.computeIfAbsent(from, k -> new LinkedList<>()).add(edge);
                iterator.remove();
            }
        }
    }

    public void restoreEdges() {
        for (Map.Entry<Integer, List<Edge>> entry : removedEdges.entrySet()) {
            int from = entry.getKey();
            List<Edge> edges = entry.getValue();
            adjacencyList.get(from).addAll(edges);
        }
        removedEdges.clear();
    }

    public Flight getFlight(int vertex) {
        for (Edge edge : adjacencyList.get(vertex)) {
            if (edge != null) {
                return edge.flight;
            }
        }
        return null;
    }
    public int calculateLayoverTime(Flight previousFlight, Flight nextFlight) {
        long layoverMinutes = Duration.between(previousFlight.getArrivalTime(), nextFlight.getDepartureTime()).toMinutes();
        return (int)layoverMinutes;
    }
    public void printFlightsByAirport(String airportCode) {
        Set<Flight> printedFlights = new HashSet<>();
        System.out.printf("%-15s %-15s %-40s %-40s %-10s\n", "Departure", "Destination", "Depart Time", "Arrival Time", "Cost");
        for (int i = 0; i < numVertices; i++) {
            List<Edge> edges = adjacencyList.get(i);
            for (Edge edge : edges) {
                if (edge.flight.getDepartureAirport().getCity().equals(airportCode)| edge.flight.getArrivalAirport().getCity().equals(airportCode) ) {
                   printedFlights.add(edge.flight);
                }
            }
        }
        for(Flight fligh:printedFlights)
        {
            System.out.println(fligh.toString());
        }
    }
    public void printFlightsByParameters(String departureAirportCode, String arrivalAirportCode, LocalDate departureDate) {
        if (departureAirportCode == null || arrivalAirportCode == null || departureDate == null) {
            throw new IllegalArgumentException("All parameters must be provided");
        }
        Set<Flight> printedFlights = new HashSet<>();
        System.out.printf("%-15s %-15s %-40s %-40s %-10s\n", "Departure", "Destination", "Depart Time", "Arrival Time", "Cost");
        for (int i = 0; i < numVertices; i++) {
            List<Edge> edges = adjacencyList.get(i);
            for (Edge edge : edges) {
                Flight flight = edge.flight;
                LocalDate flightDepartureDate = flight.getDepartureTime().toLocalDate();
                if (flight.getDepartureAirport().getCity().equals(departureAirportCode) &&
                        flight.getArrivalAirport().getCity().equals(arrivalAirportCode) &&
                        flightDepartureDate.isEqual(departureDate)) {
                    printedFlights.add(edge.flight);
                }
            }
        }
        for(Flight fligh:printedFlights)
        {
            System.out.println(fligh.toString());
        }
    }
    public List<List<Integer>> findBestRoutes(String departureAirportCode, String arrivalAirportCode,String weightPriority)
    {
        int departureId=0;
        int destinationId =0;
        System.out.printf("%-15s %-15s %-40s %-40s %-10s\n", "Departure", "Destination", "Depart Time", "Arrival Time", "Cost");
        for (int i = 0; i < numVertices; i++) {
            List<Edge> edges = adjacencyList.get(i);
            for (Edge edge : edges) {
                Flight flight = edge.flight;
                if (flight.getDepartureAirport().getCity().equals(departureAirportCode)&&flight.getArrivalAirport().getCity().equals(arrivalAirportCode))
                {
                    departureId = flight.getDepartureAirport().getId();
                    destinationId = flight.getArrivalAirport().getId();
                }
            }
        }
        YensKShortestPaths yens = new YensKShortestPaths();
        if(departureId!=0&&destinationId!=0)
            return yens.yenKSP(this,departureAirportCode,  arrivalAirportCode,3,weightPriority);
        else
            return null;

    }

    public static class Edge {
        int vertex;
        Flight flight;
        double weight;

        int layoverTime;

        Edge(int vertex, Flight flight, String weightPrioriry, int layoverTime) {
            this.vertex = vertex;
            this.flight = flight;
            this.weight = computeWeight(weightPrioriry);
            this.layoverTime= layoverTime;

        }

        public double computeWeight(String weightPriority) {
            int duration = timeDifferenceInMinutes(flight.getDepartureTime(), flight.getArrivalTime());
            double weightedValue = 0;
            switch (weightPriority) {
                case "cost":
                    weightedValue = flight.getCost()/ 10;
                    break;
                case "duration":
                    weightedValue = duration/60.0;
                    break;
                default:
                    weightedValue = flight.getCost();
            }
            return weightedValue;
        }
    }

        public static int timeDifferenceInMinutes(ZonedDateTime time1, ZonedDateTime time2) {
            return (int)Duration.between(time2,time1).toMinutes();
        }
   }

