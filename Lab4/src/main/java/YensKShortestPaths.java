import java.util.*;

public class YensKShortestPaths {

    /*    public List<List<Integer>> yenKSP(Graph graph, int source, int target, int K, String weightPriority) {
            List<List<Integer>> A = new ArrayList<>();
            PriorityQueue<CandidatePath> B = new PriorityQueue<>(Comparator.comparingInt(CandidatePath::getCost));

            List<Integer> firstPath = DijkstraAlgorithm.getShortestPathTo(target, DijkstraAlgorithm.dijkstra(graph, source,weightPriority));
            A.add(firstPath);

            for (int k = 1; k < K; k++) {
                List<Integer> prevPath = A.get(k - 1);

                for (int i = 0; i < prevPath.size() - 1; i++) {
                    int spurNode = prevPath.get(i);
                    List<Integer> rootPath = prevPath.subList(0, i + 1);

                    for (List<Integer> path : A) {
                        if (path.size() > i && rootPath.equals(path.subList(0, i + 1))) {
                            graph.removeEdge(path.get(i), path.get(i + 1));
                        }
                    }

                    int[] spurPrevious = DijkstraAlgorithm.dijkstra(graph, spurNode,weightPriority);
                    List<Integer> spurPath = DijkstraAlgorithm.getShortestPathTo(target, spurPrevious);

                    if (!spurPath.isEmpty() && spurPath.get(0) == spurNode) {
                        List<Integer> totalPath = new ArrayList<>(rootPath);
                        totalPath.addAll(spurPath.subList(1, spurPath.size()));
                        B.add(new CandidatePath(totalPath));
                    }

                    // Restore edges to graph
                    graph.restoreEdges();
                }

                if (B.isEmpty()) {
                    break;
                }

                A.add(B.poll().getPath());
            }

            return A;
        }
*/
    public List<List<Integer>> yenKSP(Graph graph, String sourceCity, String targetCity, int K, String weightPriority) {
        int sourceVertex = getVertexByCity(graph, sourceCity);
        int targetVertex = getVertexByCity(graph, targetCity);

        if (sourceVertex == -1 || targetVertex == -1) {
            return Collections.emptyList();
        }

        List<List<Integer>> A = new ArrayList<>();
        PriorityQueue<CandidatePath> B = new PriorityQueue<>(Comparator.comparingInt(CandidatePath::getCost));

        List<Integer> firstPath = DijkstraAlgorithm.getShortestPathTo(targetVertex, DijkstraAlgorithm.dijkstra(graph, sourceVertex, weightPriority));
        A.add(firstPath);

        for (int k = 1; k < K; k++) {
            List<Integer> prevPath = A.get(k - 1);

            for (int i = 0; i < prevPath.size() - 1; i++) {
                int spurNode = prevPath.get(i);
                List<Integer> rootPath = prevPath.subList(0, i + 1);

                for (List<Integer> path : A) {
                    if (path.size() > i && rootPath.equals(path.subList(0, i + 1))) {
                        graph.removeEdge(path.get(i), path.get(i + 1));
                    }
                }

                int[] spurPrevious = DijkstraAlgorithm.dijkstra(graph, spurNode, weightPriority);
                List<Integer> spurPath = DijkstraAlgorithm.getShortestPathTo(targetVertex, spurPrevious);

                if (!spurPath.isEmpty() && spurPath.get(0) == spurNode) {
                    List<Integer> totalPath = new ArrayList<>(rootPath);
                    totalPath.addAll(spurPath.subList(1, spurPath.size()));
                    B.add(new CandidatePath(totalPath));
                }

                // Restore edges to graph
                graph.restoreEdges();
            }

            if (B.isEmpty()) {
                break;
            }

            A.add(B.poll().getPath());
        }

        return A;
    }

    private int getVertexByCity(Graph graph, String city) {
        for (int i = 0; i < graph.numVertices; i++) {
            if (graph.getFlight(i).getDepartureAirport().getCity().equals(city)) {
                return i;
            }
            else if (graph.getFlight(i).getArrivalAirport().getCity().equals(city)) {
                return i+10;
            }
        }
        return -1;
    }


        public static class CandidatePath {
            private final List<Integer> path;
            private final int cost;

            public CandidatePath(List<Integer> path) {
                this.path = new ArrayList<>(path);
                this.cost = path.size();
            }

            public List<Integer> getPath() {
                return path;
            }

            public int getCost() {
                return cost;
            }
        }
}
